FROM debian:buster as alfresco_installer

WORKDIR /work

RUN apt-get update && apt-get -y install wget expect
RUN wget https://download.alfresco.com/release/community/201707-build-00028/alfresco-community-installer-201707-linux-x64.bin -O alfresco-installer --no-verbose
RUN chmod +x alfresco-installer
COPY alfresco-installer.exp /work
RUN /work/alfresco-installer.exp


FROM debian:buster

COPY --from=alfresco_installer /opt/alfresco-community /opt/alfresco-community
WORKDIR /work
RUN apt-get update && apt-get -y upgrade
RUN apt-get update && apt-get -y install curl git python3 python3-pip unzip
RUN curl -sL https://deb.nodesource.com/setup_14.x | bash -
RUN apt-get update && apt-get -y install nodejs
RUN npm install -g npm
RUN npm install -g @angular/cli
RUN apt-get update && apt-get -y install software-properties-common wget
RUN wget -qO - https://adoptopenjdk.jfrog.io/adoptopenjdk/api/gpg/key/public | apt-key add -
RUN add-apt-repository --yes https://adoptopenjdk.jfrog.io/adoptopenjdk/deb/
RUN apt-get update && apt-get -y install adoptopenjdk-8-hotspot ant
RUN pip3 install ansible==2.9.26

RUN git config --global user.email "development@tib.eu"
RUN git config --global user.name "edu-sharing-builder"

ENV LANG de_DE.UTF-8
ENV JAVA_TOOL_OPTIONS=-Dfile.encoding=UTF8

COPY build.sh /work

CMD ["/work/build.sh"]