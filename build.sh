#!/bin/bash

EDU_REPO_DIR=/work/Edu-Sharing

if [ ! -d ${EDU_REPO_DIR} ]; then
  echo "Missing edu-sharing repository. Please make sure that it is mounted as docker volume under ${EDU_REPO_DIR}"
  echo "example: docker run -v /local/path/Edu-Sharing:${EDU_REPO_DIR} --rm edu-sharing-build"
  exit 1
fi

# --------------
# Frontend Build
# --------------
cd ${EDU_REPO_DIR}/Frontend
npm install

# check if commit is contained in history, that contains change in build scripts
# https://github.com/edu-sharing/Edu-Sharing/commit/e88522a8aa660dccf4025433449eb868b735e541
FRONTEND_BUILD_PROCESS_VERSION=1
MB_RESULT_e88522a=$(git merge-base e88522a8aa660dccf4025433449eb868b735e541 HEAD)
if [ $? -ne 0 ]; then
  echo "e88522a not contained -> using old build"
  FRONTEND_BUILD_PROCESS_VERSION=1
elif [ "$MB_RESULT_e88522a" = "e88522a8aa660dccf4025433449eb868b735e541" ]; then
  echo "e88522a is before curhead -> new build"
  FRONTEND_BUILD_PROCESS_VERSION=2
else
  echo "e88522a is after curhead -> old build"
  FRONTEND_BUILD_PROCESS_VERSION=1
fi
if [ "$FRONTEND_BUILD_PROCESS_VERSION" = "1" ]; then
  ng build --prod="true"
  sed -i -e 's,<base href="/">,<base href="/edu-sharing/">,g' dist/index.html
fi
if [ "$FRONTEND_BUILD_PROCESS_VERSION" = "2" ]; then
  npm run build
fi

# -------------
# Backend Build
# -------------
cd ${EDU_REPO_DIR}/Backend
if [ ! -e ${EDU_REPO_DIR}/Backend/ng ]; then
  echo "ng does not exists -> creating link"
  ln -s ../Frontend/dist ng
fi
if [ ! -f ${EDU_REPO_DIR}/Backend/build.root.properties ]; then
  echo "user build.properties does not exist -> creating it"
  echo "webserver.home=/opt/alfresco-community/tomcat" > build.root.properties
fi
ant deploy
ant release