# Build edu-sharing in a docker container

* Docker must be set up on your machine.
* You need a copy of [edu-sharing](https://github.com/edu-sharing/Edu-Sharing) on your machine.
* Your copy of edu-sharing has to be mounted as docker volume under _/work/Edu-Sharing_
* Build via
```
docker run -v /local/path/to/Edu-Sharing:/work/Edu-Sharing --rm registry.gitlab.com/tibhannover/oer/edu-sharing-builder:latest
```
* => You find the final release-zip-file in _/local/path/to/Edu-Sharing/Backend/release_